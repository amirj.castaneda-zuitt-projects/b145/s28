// What is Node.js?
// It is an open source server environment. Uses JS on the server.
// It uses asynchronous programming.
// Asynchronous programming
// 	A thread is a single process that a program can use to complete
// tasks. Each thread can only do a single task at once. Ex. Task A -> Task B -> Task C
// Each task will be run sequentially. A task has to complete before the next one can be started.
// JavaScript is single-threaded traditionally.
// Web workers allow you to send some of the JS processing off to a separate thread.
// Workers can't access the DOM. It can basically just do the number crunching.

// How Node.JS handles a file request
// 1. sends the task to the computer's file system.
// 2. Ready to handle the next request.
// 3. When the file system has opened and read the file, the server returns the content
// to the client. Node.js eliminates the waiting and continues with the next request.

// Node.js has several built-in modules.
// // To include a module, use the require() function.
// let http = require('http'); 
// // Now, the application has access to the HTTP module and is now able to create a server.
// let dt = require('./firstmodule.js')
// let url = require('url');
// let fs = require('fs');

// Create a server object.
// http.createServer(function(request, response) {
// 	response.writeHead(200, {'Content-Type': 'text/html'}); // tells the browser to display the response from the server as an HTML
// 	var q = url.parse(request.url, true).query;
// 	var txt = q.year + ' ' + q.month;
// 	response.end(txt); // ends the response
// }).listen(8080);

// Function passed into the createServer will be executed 
// when someone tires to access the port 8080

// http.createServer(function (req, res) {
//   fs.readFile('C:/Users/User/Desktop/zuitt-projects/full-stack-webdev-in-js/Capstone Project - 1/index.html', function(err, data) {
//     res.writeHead(200, {'Content-Type': 'text/html'});
//     res.write(data);
//     return res.end();
//   });
// }).listen(8080);

let uc = require('upper-case');
let http = require('http');
http.createServer(function(req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(uc.upperCase('Hello World!'));
	res.end();
}).listen(8080);